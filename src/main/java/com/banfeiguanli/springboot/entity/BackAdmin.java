package com.banfeiguanli.springboot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

//后台管理员
@Data
public class BackAdmin {
    private Integer adminID;
    private String adminName;
    private String adminAccount;
    @JsonIgnore
    private String adminPWD;
    private String adminPhone;
    private String adminEmail;
}
