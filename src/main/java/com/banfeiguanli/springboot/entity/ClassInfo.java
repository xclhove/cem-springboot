package com.banfeiguanli.springboot.entity;

import lombok.Data;

//班级
@Data
public class ClassInfo {
    private Integer classID;
    private Integer userID;
    private String className;
    private String userName;
    private Integer classSize;
    private String isBan;
    private Float classExBala;
}
