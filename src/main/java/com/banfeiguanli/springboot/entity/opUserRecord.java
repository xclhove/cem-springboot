package com.banfeiguanli.springboot.entity;

import lombok.Data;

import java.sql.Time;

//管理用户记录
@Data
public class opUserRecord {
    private Integer recordID;
    private String opTime;
    private Integer uOpType;
    private Integer userID;
    private Integer backAdminID;
}
