package com.banfeiguanli.springboot.entity;

import lombok.Data;
//缴费项目
@Data
public class PaymentProject {
    private Integer payProjID;
    private Integer classID;
    private String releaseUser;
    private String payProjTitle;
    private String payProjBrief;
    private Float payProjAmount;
    private String releaseTime;
    private String deadLine;
    private String isDelete;
    private String isPay;
}
