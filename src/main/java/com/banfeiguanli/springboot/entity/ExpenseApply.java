package com.banfeiguanli.springboot.entity;

import lombok.Data;

import java.sql.Time;

//班费申请
@Data
public class ExpenseApply {
    private String applyUserName;
    private String auditUserName;
    private Integer expApplyID;
    private Float applyAmount;
    private String applyTime;
    private String auditTime;
    private String applyReason;
    private String status;
    private Float beforeClassExp;
    private Integer userID;
    private Integer classID;
    private Integer classAdminID;

}
