package com.banfeiguanli.springboot.entity;

import lombok.Data;

import java.sql.Time;

//管理班级记录
@Data
public class OpClassRecord {
    private Integer recordID;
    private String opTime;
    private Integer cOpType;
    private Integer classID;
    private Integer backAdminID;
}
