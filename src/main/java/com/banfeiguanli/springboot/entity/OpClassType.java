package com.banfeiguanli.springboot.entity;

import lombok.Data;

//管理班级类型
@Data
public class OpClassType {
    private Integer opClassTypeID;
    private String typeName;
    private Integer durationDay;
}
