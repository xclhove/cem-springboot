package com.banfeiguanli.springboot.entity;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ClassExpenseVariation {
    private Integer userID;
    private String userName;
    private Integer classID;
    private String time;
    private Float classExpense;
    private Float amount;
}
