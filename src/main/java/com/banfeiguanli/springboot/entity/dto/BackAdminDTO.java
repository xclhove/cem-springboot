package com.banfeiguanli.springboot.entity.dto;

import lombok.Data;

@Data
public class BackAdminDTO {
    private String adminName;
    private String adminAccount;
    private String adminPWD;
    private String adminPWD2;
    private String adminPhone;
    private String adminEmail;
    private String registerKey;
}
