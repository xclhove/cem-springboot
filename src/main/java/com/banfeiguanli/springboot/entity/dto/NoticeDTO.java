package com.banfeiguanli.springboot.entity.dto;
import lombok.Data;

@Data
public class NoticeDTO {
    private Integer noticeID;
    private Integer adminID;
    private String noticeTitle;
    private String releaseTime;
    private String noticeContent;
    private String token;
}
