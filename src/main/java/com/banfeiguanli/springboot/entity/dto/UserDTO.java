package com.banfeiguanli.springboot.entity.dto;

import lombok.Data;

//接收前端登录请求数据
@Data
public class UserDTO {
    private Integer userID;
    private String userName;
    private String userAccount;
    private String userPWD;
    private String userPWD2;
    private String userPhone;
    private String userEmail;
}