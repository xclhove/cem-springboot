package com.banfeiguanli.springboot.entity.dto;

import lombok.Data;

@Data
public class ClassNoticeDTO {
    private Integer claNoticeID;
    private String noticeTitle;
    private String releaseTime;
    private String noticeContent;
    private String token;
}

