package com.banfeiguanli.springboot.entity.dto;

import lombok.Data;

@Data
public class ExpenseApplyDTO {
    private Integer expApplyID;
    private Float applyAmount;
    private String applyTime;
    private String auditTime;
    private String applyReason;
    private String status;
    private Float beforeClassExp;
    private Integer userID;
    private Integer classAdminID;
    private String token;
}
