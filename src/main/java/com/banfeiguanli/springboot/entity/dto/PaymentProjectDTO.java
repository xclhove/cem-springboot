package com.banfeiguanli.springboot.entity.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class PaymentProjectDTO {
    private Integer payProjID;
    private String releaseUser;
    private String payProjTitle;
    private String payProjBrief;
    private Float payProjAmount;
    private Timestamp releaseTime;
    private Timestamp deadLine;
    private String isDelete;
    //班级管理员token
    private String token;
    Integer durationDay;
}
