package com.banfeiguanli.springboot.entity.dto;

import lombok.Data;

@Data
public class ClassDTO {
    private int classID;
    private int userID;
    private String userToken;
}
