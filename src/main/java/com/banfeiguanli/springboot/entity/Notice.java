package com.banfeiguanli.springboot.entity;

import lombok.Data;

import java.sql.Time;
//全体公告
@Data
public class Notice {
    private Integer noticeID;
    private String noticeTitle;
    private String releaseTime;
    private String noticeContent;
    private Integer adminID;
    private String releaseUser;
}
