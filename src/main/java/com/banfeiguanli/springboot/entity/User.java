package com.banfeiguanli.springboot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
//用户
@Data
public class User {
    private Integer userID;
    private Integer classID;
    private String userName;
    private String userAccount;
    @JsonIgnore
    private String userPWD;
    private String userPhone;
    private String userEmail;
    private String isBan;
    private  String isClassAdmin;
    private  String className;
}