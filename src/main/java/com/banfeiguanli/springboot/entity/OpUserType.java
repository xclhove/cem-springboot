package com.banfeiguanli.springboot.entity;

import lombok.Data;

//管理用户类型
@Data
public class OpUserType {
    private Integer opUserTypeID;
    private String typeName;
    private Integer durationDay;
}
