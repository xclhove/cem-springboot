package com.banfeiguanli.springboot.entity;

import lombok.Data;

import java.sql.Time;

//缴费记录
@Data
public class PaymentRecord {
    private String payRecordExp;
    private Float beforeClassExp;
    private Integer userID;
    private Integer payProjID;
}
