package com.banfeiguanli.springboot.entity;

import lombok.Data;
import java.sql.Time;
//班级管理员
@Data
public class ClassAdmin {
    private Integer classAdminID;
    private String startTime;
    private String endTime;
    private Integer userID;
    private String userName;
    private Integer classID;
    private Integer className;
}
