package com.banfeiguanli.springboot.entity;

import lombok.Data;

import java.sql.Time;

//班级通知
@Data
public class ClassNotice {
    private Integer ClaNoticeID;
    private String noticeTitle;
    private String noticeContent;
    private String releaseTime;
    private Integer classAdminID;
    private Integer userID;
    private String userName;
    private Integer classID;
    private String className;
}