package com.banfeiguanli.springboot.common;

public interface Constant {
    //成功
    String CODE_200 = "200";
    //系统错误
    String CODE_500 = "500";
    //班费余额不足
    String CODE_501 = "501";
    //无班级
    String CODE_510 = "510";
    //其它业务异常
    String CODE_600 = "600";
    //token为空，登录失效
    String CODE_601 = "601";
    //token中用户不存在
    String CODE_602 = "602";
    //数据库中用户不存在
    String CODE_603 = "603";
    //token验证失败
    String CODE_604 = "604";
    //后台管理员不存在
    String CODE_605 = "605";
    //token错误
    String CODE_606 = "606";
    //数据库中后台管理员不存在
    String CODE_607 = "607";
    //所加入的班级不存在
    String CODE_610 = "610";
    //加入班级失败
    String CODE_611 = "611";
    //未加入任何班级
    String CODE_612 = "612";
    //已加入班级
    String CODE_613 = "613";
    //非班级管理员
    String CODE_620 = "620";
    //无权解散其他班级
    String CODE_621 = "621";
    //删除班级失败
    String CODE_622 = "622";
    //班费余额大于1禁止解散
    String CODE_623 = "623";
    //移除班级管理员失败
    String CODE_624 = "624";
    //移除班级成员失败
    String CODE_625 = "625";
    //无权移除其他班级的成员
    String CODE_630 = "630";
    //管理员无法移除自己
    String CODE_631 = "631";
    //缴费项目不存在
    String CODE_640 = "640";
    //无权删除其他班级的项目
    String CODE_641 = "641";
    //缴费金额非法
    String CODE_642 = "642";
    //发布缴费项目失败
    String CODE_643 = "643";
    //缴费失败
    String CODE_644 = "644";
    //更新班费余额失败
    String CODE_645 = "645";
    //后台管理员注册：密钥错误
    String CODE_650 = "650";
    //后台管理员注册：密码与确认密码不一致
    String CODE_651 = "651";
    //后台管理员注册：手机号已被注册
    String CODE_652 = "652";
    //后台管理员注册：账号已被注册
    String CODE_653 = "653";
    //后台管理员注册失败(插入数据失败)
    String CODE_654 = "654";
    //后台管理员登录：账号不存在
    String CODE_655 = "655";
    //后台管理员登录：密码错误
    String CODE_656 = "656";
    //班费申请失败
    String CODE_660 = "660";
    //撤销班费申请失败
    String CODE_661 = "661";
    //token错误后台管理员不存在
    String CODE_670 = "670";
    //创建班级失败
    String CODE_701 = "701";
    //获取公钥失败
    String CODE_1000 = "1000";
    //获取私钥失败
    String CODE_1001 = "1001";
    //解密失败
    String CODE_1002 = "1002";
    //请求成功
    String SUCCESS = "success";
    //请求失败
    String ERROR = "error";
    //用户token前缀
    String USER = "users";
    //管理员token前缀
    String ADMIN = "admin";
    //后台管理员注册密钥
    String registerKey = "banfeiguanlixitong";
}
