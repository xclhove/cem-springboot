package com.banfeiguanli.springboot.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private String code;
    private String message;
    private String type;
    private Object data;

    public static Result success() {
        return new Result(Constant.CODE_200, "requestSuccess!", Constant.SUCCESS, null);
    }

    public static Result success(Object data) {
        return new Result(Constant.CODE_200, "requestSuccess!", Constant.SUCCESS, data);
    }

    public static Result success(String message, Object data) {
        return new Result(Constant.CODE_200, message, Constant.SUCCESS, data);
    }

    public static Result success(String code, String message, Object data) {
        return new Result(code, message, Constant.SUCCESS, data);
    }

    public static Result error() {
        return new Result(Constant.CODE_600, "error!", Constant.ERROR, null);
    }

    public static Result error(String message) {
        return new Result(Constant.CODE_600, message, Constant.ERROR, null);
    }

    public static Result error(String code, String message) {
        return new Result(code, message, Constant.ERROR, null);
    }

    public static Result error(String code, String message, Object data) {
        return new Result(code, message, Constant.ERROR, data);
    }
}
