package com.banfeiguanli.springboot.service;

import com.banfeiguanli.springboot.common.Constant;
import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.ClassNotice;
import com.banfeiguanli.springboot.entity.dto.ClassNoticeDTO;
import com.banfeiguanli.springboot.entity.dto.NoticeDTO;
import com.banfeiguanli.springboot.mapper.ClassNoticeMapper;
import com.banfeiguanli.springboot.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClassNoticeService {
    @Autowired
    private ClassNoticeMapper classNoticeMapper;

    //获取班级所有通知
    public Result getAllClassNotice(String token) {
        Map<String, Object> data = new HashMap<>();
        //从token中取出userId
        int userID = TokenUtils.getIdByToken(token);
        //利用userID查询通知信息
        List<ClassNotice> noticeList = classNoticeMapper.getClassNotice(userID);
        data.put("classNotice", noticeList);
        return Result.success(data);
    }
    //创建班级通知
    public Result createClassNotice(ClassNoticeDTO classNotice){
        int  userID = TokenUtils.getIdByToken(classNotice.getToken());
        String noticeTitle = classNotice.getNoticeTitle();
        String noticeContent = classNotice.getNoticeContent();
        if(noticeTitle.trim().equals("")){
            return Result.error("500","通知标题不能为空");
        }
        if(noticeContent.trim().equals("")){
            return Result.error("500","通知内容不能为空");
        }
        int insertNoticeNum = classNoticeMapper.createClassNotice(noticeTitle,noticeContent,userID);
        if(insertNoticeNum != 1){
            return Result.error("500","创建班级通知失败",null);
        }
        return Result.success("200","成功创建班级通知",null);
    }

    public Result searchClassNotice(String classNoticeTitle) {
        return Result.error("500","未查询到类似标题公告！");
    }

    public Result deleteClassNotice(int classNoticeID) {
        int deleteRowNum = classNoticeMapper.deleteClassNotice(classNoticeID);
        if(deleteRowNum != 1){
            return Result.error(Constant.CODE_500,"删除失败");
        }
        return Result.success(Constant.CODE_200,"删除成功","");
    }
    //修改通知
    public Result changeClassNotice(NoticeDTO classNotice) {
        int classNoticeID = classNotice.getNoticeID();
        String classNoticeTitle = classNotice.getNoticeTitle();
        String classNoticeContent = classNotice.getNoticeContent();
        if((classNoticeTitle.trim().equals("")) || (classNoticeContent.trim().equals(""))){
            return  Result.error(Constant.CODE_500,"通知标题和内容不能为空");
        }
        int changeClassNoticeNum = classNoticeMapper.changeClassNotice(classNoticeID,classNoticeTitle,classNoticeContent);
        if(changeClassNoticeNum != 1){
            return  Result.error(Constant.CODE_500,"修改失败");
        }
        return  Result.success(Constant.CODE_200,"修改成功","");
    }
    //获取班级通知总条数
    public int getAllClassNoticeNum(String token, String searchNoticeTitle) {
        //从token中取出userId
        int  userID = TokenUtils.getIdByToken(token);
        //利用userID查询通知信息
        int classNoticeNum = classNoticeMapper.getAllClassNoticeNum(userID, searchNoticeTitle);
        return classNoticeNum;
    }
    //获得满足搜索条件班级通知
    public Result getAllClassNotice(String token, String searchNoticeTitle, int classNoticePageSize, int classNoticePageNumber) {
        Map<String, Object> data = new HashMap<>();
        //从token中取出userId
        int  userID = TokenUtils.getIdByToken(token);
        int classNoticeTotalNumber = 0;
        //利用userID查询通知信息
        classNoticePageNumber = (classNoticePageNumber - 1) * classNoticePageNumber;
        classNoticeTotalNumber = getAllClassNoticeNum(token, searchNoticeTitle);
        List<ClassNotice> noticeList = classNoticeMapper.getAllClassNotice(userID, searchNoticeTitle, classNoticePageNumber, classNoticePageSize);
        classNoticeTotalNumber = getAllClassNoticeNum(token, searchNoticeTitle);
        data.put("classNoticeTotalNum", classNoticeTotalNumber);
        data.put("classNotice",noticeList);
        if(searchNoticeTitle.trim().equals("")){
            return Result.success("成功加载全部班级通知",data);
        }
        return Result.success("查询成功",data);
    }

    //获取班级通知(后台管理)
    public Result getClassNoticeForBack(String token, Integer pageNumber, Integer pageSize, String searchTitle) {
        Map<String, Object> data = new HashMap<>();
        //提取adminID
        Integer adminID = TokenUtils.getIdByToken(token);
        //查询
        pageNumber = (pageNumber - 1) * pageSize;
        List<ClassNotice> allClassNotice = classNoticeMapper.getClassNoticeForBack(pageNumber, pageSize, searchTitle);
        data.put("allClassNotice", allClassNotice);
        //统计总数
        Integer allClassNoticeTotal = classNoticeMapper.countAllClassNoticeTotal(searchTitle);
        data.put("allClassNoticeTotal", allClassNoticeTotal);
        return Result.success(data);
    }

    //删除班级通知(后台管理)
    public Result deleteClassNoticeForBack(String token, Integer calNoticeID) {
        //提取adminID
        Integer adminID = TokenUtils.getIdByToken(token);
        //删除数据
        Integer deleteRow = classNoticeMapper.deleteClassNoticeForBack(calNoticeID);
        if (deleteRow != 1) {
            return Result.error("删除失败！");
        }
        return Result.success("删除成功！", null);
    }
}
