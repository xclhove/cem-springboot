package com.banfeiguanli.springboot.service;

import com.banfeiguanli.springboot.entity.ClassAdmin;
import com.banfeiguanli.springboot.mapper.ClassAdminMapper;
import com.banfeiguanli.springboot.mapper.ClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassAdminService {
    @Autowired
    private ClassAdminMapper classAdminMapper;

    ClassAdmin getClassAdmin(int userID) {
        return classAdminMapper.getClassAdmin(userID).get(0);
    }
}
