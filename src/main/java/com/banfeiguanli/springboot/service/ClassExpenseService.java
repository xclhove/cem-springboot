package com.banfeiguanli.springboot.service;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.ClassExpenseVariation;
import com.banfeiguanli.springboot.mapper.ClassAdminMapper;
import com.banfeiguanli.springboot.mapper.ClassExpenseMapper;
import com.banfeiguanli.springboot.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClassExpenseService {
    @Autowired
    private ClassExpenseMapper classExpenseMapper;
    @Autowired
    private ClassAdminMapper classAdminMapper;

    //获取班费变化
    public Result getClassExpenseVariation(String token, Integer pageNumber, Integer pageSize, String searchUser) {
        Map<String, Object> data = new HashMap<>();
        Integer userID = TokenUtils.getIdByToken(token);
        pageNumber = (pageNumber - 1) * pageSize;
        List<ClassExpenseVariation> classExpenseVariations = classExpenseMapper.getClassExpenseVariation(userID, pageNumber, pageSize, searchUser);
        data.put("classExpenseVariations", classExpenseVariations);
        //统计总数
        Integer variationTotal = classExpenseMapper.countVariationTotal(userID, searchUser);
        data.put("variationTotal", variationTotal);
        //获取当前班费余额
        Float classExpense = classExpenseMapper.getClassExpense(userID);
        data.put("classExpense", classExpense);
        return Result.success(data);
    }
}
