package com.banfeiguanli.springboot.service;

import com.banfeiguanli.springboot.common.Constant;
import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.BackAdmin;
import com.banfeiguanli.springboot.entity.dto.BackAdminDTO;
import com.banfeiguanli.springboot.mapper.BackAdminMapper;
import com.banfeiguanli.springboot.utils.MyPasswordEncoder;
import com.banfeiguanli.springboot.utils.RsaUtil;
import com.banfeiguanli.springboot.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BackAdminService {
    @Autowired
    private BackAdminMapper backAdminMapper;
    //通过adminId获取后台管理员信息
    public List<BackAdmin> getBackAdminInfo(int adminID) {
        return backAdminMapper.getBackAdminInfo(adminID);
    }
    //后台管理员登录
    public Result login(BackAdminDTO backAdminDTO) {
        Map<String, Object> data = new HashMap<>();
        //使用私钥解密账号密码
        String adminAccount = RsaUtil.decrypt(backAdminDTO.getAdminAccount()).trim();
        String adminPWD = RsaUtil.decrypt(backAdminDTO.getAdminPWD()).trim();
        //判断账号是否存在
        if (!adminAccountExist(adminAccount)){
            return Result.error(Constant.CODE_655,"账号不存在!");
        }
        //密码加密
        adminPWD = new MyPasswordEncoder().encode(adminAccount, adminPWD);
        //利用账号密码查询后台管理员信息
        List<BackAdmin> backAdminList = backAdminMapper.login(adminAccount, adminPWD);
        if (backAdminList.isEmpty()) {
            return Result.error(Constant.CODE_656,"密码错误!");
        }
        BackAdmin adminInfo = backAdminList.get(0);
        data.put("adminInfo", adminInfo);
        //利于adminID与密码生成token
        String token = TokenUtils.generateAdminToken(adminInfo.getAdminID().toString(), adminInfo.getAdminPWD());
        data.put("token", token);
        return Result.success("登录成功！", data);
    }
    //后台管理员注册
    public Result register(BackAdminDTO backAdminDTO) {
        Map<String, Object> data = new HashMap<>();
        //使用私钥解密账号密码
        String adminAccount = RsaUtil.decrypt(backAdminDTO.getAdminAccount()).trim();
        String adminPWD = RsaUtil.decrypt(backAdminDTO.getAdminPWD()).trim();
        String adminPWD2 = RsaUtil.decrypt(backAdminDTO.getAdminPWD2()).trim();
        String registerKey = RsaUtil.decrypt(backAdminDTO.getRegisterKey()).trim();
        //判断注册密钥是否正确
        if (!registerKey.equals(Constant.registerKey)){
            return Result.error(Constant.CODE_650,"密钥错误!");
        }
        //判断密码与确认密码是否一致
        if (!adminPWD.equals(adminPWD2)) {
            return Result.error(Constant.CODE_651,"密码与确认密码不一致!");
        }
        //判断账号是否重复
        if (adminAccountExist(adminAccount)){
            return Result.error(Constant.CODE_653,"账号已被注册!");
        }
        //判断手机号是否重复
        if (adminPhoneExist(backAdminDTO.getAdminPhone())){
            return Result.error(Constant.CODE_652,"手机号已被注册!");
        }
        //密码加密
        adminPWD = new MyPasswordEncoder().encode(adminAccount, adminPWD);
        //数据存入对象
        backAdminDTO.setAdminAccount(adminAccount);
        backAdminDTO.setAdminPWD(adminPWD);
        //注册，插入数据到数据库
        Integer changeRow = backAdminMapper.register(backAdminDTO);
        if (changeRow != 1) {
            return Result.error(Constant.CODE_654, "注册失败!");
        }
        //
        data.put("backAdminDTO", backAdminDTO);
        return Result.success("注册成功！", data);
    }

    //判断账号是否重复
    private boolean adminAccountExist(String adminAccount) {
        List<BackAdmin> backAdminList = backAdminMapper.getBackAdminInfoByAccount(adminAccount);
        return !backAdminList.isEmpty();
    }

    //判断手机号是否重复
    private boolean adminPhoneExist(String adminPhone) {
        List<BackAdmin> backAdminList = backAdminMapper.getBackAdminInfoByPhone(adminPhone);
        return !backAdminList.isEmpty();
    }

    //获取后台管理员信息
    public Result getAdminInfo(String token) {
        Map<String, Object> data = new HashMap<>();
        //从token中提取adminID
        Integer adminID = TokenUtils.getIdByToken(token);
        //利用adminID查询信息
        List<BackAdmin> backAdminList = backAdminMapper.getAdminInfo(adminID);
        if (backAdminList.isEmpty()) {
            return Result.error(Constant.CODE_670, "token错误，请重新登录！");
        }
        data.put("adminInfo", backAdminList.get(0));
        return Result.success(data);
    }

    //通过adminID获取管理员信息
    public List<BackAdmin> getAdminInfoByAdminID(Integer adminID) {
        return backAdminMapper.getAdminInfo(adminID);
    }
}
