package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.Notice;
import com.banfeiguanli.springboot.entity.dto.NoticeDTO;
import com.banfeiguanli.springboot.service.NoticeService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    private NoticeService noticeService;

    @GetMapping("/all")
    public Result getAllNotice() {
        return noticeService.getAllNotice();
    }

    //查询全体公告(后台管理)
    @GetMapping("/info")
    public Result getNoticeInfo(@RequestParam String token,
                                @RequestParam Integer pageNumber,
                                @RequestParam Integer pageSize,
                                @RequestParam String searchTitle) {
        return noticeService.getNoticeInfo(token, pageNumber, pageSize, searchTitle);
    }

    //删除全体公告(后台管理)
    @DeleteMapping("/info")
    public Result deleteNotice(@RequestParam String token,
                               @RequestParam Integer noticeID) {
        return noticeService.deleteNotice(token, noticeID);
    }

    //发布全体公告
    @PostMapping("/info")
    public Result addNotice(@RequestBody NoticeDTO noticeDTO) {
        return noticeService.addNotice(noticeDTO);
    }
}
