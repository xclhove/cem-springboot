package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.dto.ExpenseApplyDTO;
import com.banfeiguanli.springboot.entity.dto.PaymentProjectDTO;
import com.banfeiguanli.springboot.service.PaymentProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/paymentProject")
public class PaymentProjectController {
    @Autowired
    private PaymentProjectService paymentProjectService;

    //获取班级缴费项目(班级管理员)
    @GetMapping("/info")
    public Result getClassPaymentProject(@RequestParam String token,
                                         @RequestParam Integer pageNumber,
                                         @RequestParam Integer pageSize,
                                         @RequestParam String searchTitle) {
        return paymentProjectService.getClassPaymentProject(token, pageNumber, pageSize, searchTitle);
    }

    //获取班级缴费项目(用户)
    @GetMapping("/infoForUser")
    public Result getClassPaymentProjectForUser(@RequestParam String token,
                                                @RequestParam Integer pageNumber,
                                                @RequestParam Integer pageSize,
                                                @RequestParam String searchTitle) {
        return paymentProjectService.getClassPaymentProjectForUser(token, pageNumber, pageSize, searchTitle);
    }

    //删除缴费项目
    @PostMapping("/delete")
    public Result deleteClassPaymentProject(@RequestBody PaymentProjectDTO paymentProjectDTO) {
        return paymentProjectService.deleteClassPaymentProject(paymentProjectDTO);
    }

    //发布缴费项目
    @PostMapping("/create")
    public Result createPaymentProject(@RequestBody PaymentProjectDTO paymentProjectDTO) {
        return paymentProjectService.createPaymentProject(paymentProjectDTO);
    }

    @GetMapping("/pay")
    public Result pay(@RequestParam String token, @RequestParam Integer paymentProjectID) {
        return paymentProjectService.pay(token, paymentProjectID);
    }

    //获取班费申请项目到审核申请页面(班级管理员)
    @GetMapping("/expenseApplyInfo")
    public Result getExpenseApply(@RequestParam String token,
                                  @RequestParam Integer applyMessagePageNumber,
                                  @RequestParam Integer applyMessagePageSize,
                                  @RequestParam String searchExpenseApplyReason) {
        return paymentProjectService.getExpenseApply(token, applyMessagePageNumber, applyMessagePageSize, searchExpenseApplyReason);
    }


    //拒绝班费申请
    @PostMapping("/refuse")
    public Result refuseExpenseApply(@RequestBody ExpenseApplyDTO expenseApplyDTO) {
        return paymentProjectService.refuseExpenseApply(expenseApplyDTO);
    }

    //同意班费申请
    @PostMapping("/approve")
    public Result approveExpenseApply(@RequestBody ExpenseApplyDTO expenseApplyDTO) {
        return paymentProjectService.approveExpenseApply(expenseApplyDTO);
    }
}
