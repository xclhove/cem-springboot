package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.dto.BackAdminDTO;
import com.banfeiguanli.springboot.service.BackAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/back")
public class BackAdminController {
    @Autowired
    private BackAdminService backAdminService;
    //后台管理员注册
    @PostMapping("/register")
    public Result register(@RequestBody BackAdminDTO backAdminDTO) {
        return backAdminService.register(backAdminDTO);
    }
    //后台管理员登录
    @PostMapping("/login")
    public Result login(@RequestBody BackAdminDTO backAdminDTO){
        return backAdminService.login(backAdminDTO);
    }
    //获取后台管理员信息
    @GetMapping("/info")
    public Result getAdminInfo(@RequestParam String token) {
        return backAdminService.getAdminInfo(token);
    }
}
