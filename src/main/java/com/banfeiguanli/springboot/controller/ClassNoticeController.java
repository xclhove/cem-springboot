package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.dto.ClassNoticeDTO;
import com.banfeiguanli.springboot.entity.dto.NoticeDTO;
import com.banfeiguanli.springboot.service.ClassNoticeService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/classNotice")
public class ClassNoticeController {
    @Autowired
    private ClassNoticeService classNoticeService;
    //获取改班级所有班级通知
    @GetMapping("/all")
    public Result getClassNotice(@RequestParam String token){
        return classNoticeService.getAllClassNotice(token);
    }

    //创建班级通
    @PostMapping("/createClassNotice")
    public Result createClassNotice(@RequestBody ClassNoticeDTO classNotice){
        return classNoticeService.createClassNotice(classNotice);
    }
    @GetMapping("/searchClassNotice")
    public Result searchClassNotice(@RequestParam String token,
                                    @RequestParam String searchNoticeTitle,
                                    @RequestParam int classNoticePageSize,
                                    @RequestParam int classNoticePageNumber){
        return classNoticeService.getAllClassNotice(token,searchNoticeTitle, classNoticePageSize, classNoticePageNumber);
    }
    //删除班级通知
    @GetMapping("/deleteClassNotice")
    public Result deleteClassNotice(@RequestParam int classNoticeID){
        return classNoticeService.deleteClassNotice(classNoticeID);
    }
    //修改班级通知
    @PostMapping("/changeClassNotice")
    public Result changeClassNotice(@RequestBody NoticeDTO classNotice){
        return classNoticeService.changeClassNotice(classNotice);
    }
    //获取班级通知(后台管理)
    @GetMapping("/info")
    public Result getClassNoticeForBack(@RequestParam String token,
                                        @RequestParam Integer pageNumber,
                                        @RequestParam Integer pageSize,
                                        @RequestParam String searchTitle) {
        return classNoticeService.getClassNoticeForBack(token, pageNumber, pageSize, searchTitle);
    }
    //删除班级通知(后台管理)
    @DeleteMapping("/info")
    public Result deleteClassNoticeForBack(@RequestParam String token,
                                           @RequestParam Integer claNoticeID) {
        return classNoticeService.deleteClassNoticeForBack(token, claNoticeID);
    }
}
