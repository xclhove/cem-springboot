package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.dto.ClassDTO;
import com.banfeiguanli.springboot.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/class")
public class ClassController {

    @Autowired//注入类
    private ClassService classService;

    //获取班级信息
    @GetMapping("/info")
    public Result findAll (@RequestParam int pageNumber,
                           @RequestParam int pageSize,
                           @RequestParam String searchClass) {
        return classService.findAll(pageNumber, pageSize, searchClass);
    }

    //创建班级
    @GetMapping("/createClass")
    public Result createClass(@RequestParam String className,
                              @RequestParam String token){
        return classService.createClass(className,token);
    }
    //加入班级
    @PostMapping("/join")
    public Result joinClass(@RequestBody ClassDTO classDTO){
        return classService.joinClass(classDTO);
    }

    //退出班级
    @PostMapping("/quit")
    public Result quitClass(@RequestBody ClassDTO classDTO) {
        return classService.quitClass(classDTO);
    }

    //解散班级
    @PostMapping("/disMiss")
    public Result disMissClass(@RequestBody ClassDTO classDTO) {
        return classService.disMissClass(classDTO);
    }
    //获取班级成员
    @GetMapping("/userList")
    public Result getClassUserList(@RequestParam String token,
                                   @RequestParam int pageNumber,
                                   @RequestParam int pageSize,
                                   @RequestParam String searchUser) {
        return classService.getClassUserList(token, pageNumber, pageSize, searchUser);
    }
    //移除班级成员
    @PostMapping("/removeUser")
    public Result removeUser(@RequestBody ClassDTO classDTO){
        return classService.removeUser(classDTO);
    }

    //获取班级信息(后台管理)
    @GetMapping("/infoForBack")
    public Result getClassInfoForBack(@RequestParam String token,
                                      @RequestParam Integer pageNumber,
                                      @RequestParam Integer pageSize,
                                      @RequestParam String searchClass) {
        return classService.getClassInfoForBack(token, pageNumber, pageSize, searchClass);
    }

    //禁封班级
    @PostMapping("/ban")
    public Result banClass(@RequestBody ClassDTO classDTO) {
        return classService.banClass(classDTO);
    }

    //解封班级
    @PostMapping("/unBan")
    public Result unBanClass(@RequestBody ClassDTO classDTO) {
        return classService.unBanClass(classDTO);
    }

}
