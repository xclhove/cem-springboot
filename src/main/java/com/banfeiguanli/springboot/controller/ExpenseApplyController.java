package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.dto.ExpenseApplyDTO;
import com.banfeiguanli.springboot.service.ExpenseApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/expenseApply")
public class ExpenseApplyController {
    @Autowired
    private ExpenseApplyService expenseApplyService;

    //获取用户所在班级的申请记录
    @GetMapping("/info")
    public Result getApplyRecord(@RequestParam String token,
                                 @RequestParam Integer pageNumber,
                                 @RequestParam Integer pageSize,
                                 @RequestParam String searchUser,
                                 @RequestParam String searchReason) {
        return expenseApplyService.getApplyRecord(token, pageNumber, pageSize, searchUser, searchReason);
    }
    //申请班费
    @PostMapping("/apply")
    public Result applyClassExpense(@RequestBody ExpenseApplyDTO expenseApplyDTO) {
        return expenseApplyService.applyClassExpense(expenseApplyDTO);
    }
    //撤销申请
    @GetMapping("/cancelApply")
    public Result cancelApply(@RequestParam Integer expApplyID) {
        return expenseApplyService.cancelApply(expApplyID);
    }
}
