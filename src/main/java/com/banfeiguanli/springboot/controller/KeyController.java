package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.utils.RsaUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/key")
public class KeyController {
    @GetMapping("/getPublicKey")
    public Result getPublicKey() {
        String publicKey = RsaUtil.getPublicKey();
        Map<String,Object> data = new HashMap<>();
        data.put("publicKey",publicKey);
        return Result.success(data);
    }
}
