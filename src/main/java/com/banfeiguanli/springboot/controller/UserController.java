package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.entity.dto.UserDTO;
import com.banfeiguanli.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    //注册
    @PostMapping("/register")
    public Result userRegister(@RequestBody UserDTO userDTO) {
        return userService.userRegister(userDTO);
    }

    //登录
    @PostMapping("/login")
    public Result userLogin(@RequestBody UserDTO userDTO) {
        return userService.userLogin(userDTO);
    }

    //主页信息
    @GetMapping("/info")
    public Result getUserInfo(@RequestParam String token) {
        return userService.getUserInfo(token);
    }

    //更新用户信息
    @PostMapping("/info")
    public Result updateUserInfo(@RequestBody UserDTO userDTO) {
        return userService.updateUserInfo(userDTO);
    }

    //获取用户已经加入的班级
    @GetMapping("/classInfo")
    public Result getUserClassInfo(@RequestParam String token) {
        return userService.getUserClassInfo(token);
    }

    //获取用户管理的班级
    @GetMapping("/adminClassInfo")
    public Result getAdminClassInfo(@RequestParam String token) {
        return userService.getAdminClassInfo(token);
    }

    //获取用户信息(后台管理)
    @GetMapping("/infoForBack")
    public Result getUserInfoForBack(@RequestParam String token,
                                     @RequestParam Integer pageNumber,
                                     @RequestParam Integer pageSize,
                                     @RequestParam String searchUser) {
        return userService.getUserInfoForBack(token, pageNumber, pageSize, searchUser);
    }

    //禁封用户
    @PostMapping("/ban")
    public Result banUser(@RequestBody UserDTO userDTO) {
        return userService.banUser(userDTO);
    }

    //解封用户
    @PostMapping("/unBan")
    public Result unBanUser(@RequestBody UserDTO userDTO) {
        return userService.unBanUser(userDTO);
    }
}
