package com.banfeiguanli.springboot.controller;

import com.banfeiguanli.springboot.common.Result;
import com.banfeiguanli.springboot.service.ClassExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/classExpense")
public class ClassExpenseController {
    @Autowired
    private ClassExpenseService classExpenseService;
    //获取班费变化
    @GetMapping("/info")
    public Result getClassExpenseVariation(@RequestParam String token,
                                           @RequestParam Integer pageNumber,
                                           @RequestParam Integer pageSize,
                                           @RequestParam String searchUser){
        return classExpenseService.getClassExpenseVariation(token, pageNumber, pageSize, searchUser);
    }
}
