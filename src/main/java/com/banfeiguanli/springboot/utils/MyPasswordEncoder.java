package com.banfeiguanli.springboot.utils;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

@Component
public class MyPasswordEncoder {
    //密码加密：SHA-256 + MD5 二重加密
    public String encode(String userAccount, String userPassword) {
        //对用户的账号密码进行MD5加密得到salt
        String salt = DigestUtils.md5DigestAsHex((userAccount + userPassword).getBytes());
        //SHA-256加密
        String encodePassword = new SimpleHash("SHA-256", userPassword, salt, 1024).toString();
        //MD5加密
        return DigestUtils.md5DigestAsHex(encodePassword.getBytes());
    }
}