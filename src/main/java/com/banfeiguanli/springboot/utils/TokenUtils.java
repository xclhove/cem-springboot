package com.banfeiguanli.springboot.utils;

import cn.hutool.core.date.DateUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.banfeiguanli.springboot.common.Constant;

import java.util.Date;

public class TokenUtils {

    //生成userToken
    public static String generateUserToken(String userId, String signature) {
        userId = Constant.USER + userId;
        return JWT.create().withAudience(userId) // 将 user id 保存到 token 里面
                .withExpiresAt(DateUtil.offsetHour(new Date(), 2)) //2小时后token过期
                .sign(Algorithm.HMAC256(signature)); // 以 password 作为 token 的密钥
    }

    //生成adminToken
    public static String generateAdminToken(String adminId, String signature) {
        adminId = Constant.ADMIN + adminId;
        return JWT.create().withAudience(adminId) // 将 adminId 保存到 token 里面
                .withExpiresAt(DateUtil.offsetHour(new Date(), 2)) //2小时后token过期
                .sign(Algorithm.HMAC256(signature)); // 以 password 作为 token 的密钥
    }

    //判断token类型
    public static String getTokenType(String token) throws JWTDecodeException {
        return JWT.decode(token).getAudience().get(0).substring(0, 5);
    }

    //从token中提取userId/adminId
    public static int getIdByToken(String token) throws JWTDecodeException {
        return Integer.parseInt(JWT.decode(token).getAudience().get(0).substring(5));
    }
}
