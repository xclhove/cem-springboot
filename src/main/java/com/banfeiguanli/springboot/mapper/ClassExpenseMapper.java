package com.banfeiguanli.springboot.mapper;

import com.banfeiguanli.springboot.entity.ClassExpenseVariation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ClassExpenseMapper {
    //查看班费变化
    @Select("select * from bfbh " +
            "where classID = (" +
            "select UI_ClassID from userinfo where userID = #{userID}) " +
            "and userName like concat('%', #{searchUser}, '%')" +
            "limit #{pageNumber}, #{pageSize}")
    List<ClassExpenseVariation> getClassExpenseVariation(@Param("userID") Integer userID,
                                                         @Param("pageNumber") Integer pageNumber,
                                                         @Param("pageSize") Integer pageSize,
                                                         @Param("searchUser") String searchUser);

    //统计记录总数
    @Select("select count(*) " +
            "from bfbh " +
            "where classID = (" +
            "select UI_ClassID from userinfo where userID = #{userID}) " +
            "and userName like concat('%', #{searchUser}, '%')")
    Integer countVariationTotal(@Param("userID") Integer userID,
                                @Param("searchUser") String searchUser);

    //获取当前班费余额
    @Select("select ClassExBala " +
            "from class " +
            "where ClassID = (" +
            "select UI_ClassID from userinfo where UserID = #{userID})")
    Float getClassExpense(@Param("userID") Integer userID);
}
