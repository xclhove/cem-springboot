package com.banfeiguanli.springboot.mapper;

import com.banfeiguanli.springboot.entity.Notice;
import com.banfeiguanli.springboot.entity.dto.NoticeDTO;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface NoticeMapper {

    @Select("select ReleaseTime, NoticeTitle, NoticeContent from notice order by ReleaseTime desc limit 10")
    List<Notice> getAllNotice();

    //查询全体公告(后台管理)
    @Select("select NoticeID, NoticeTitle, NoticeContent, ReleaseTime, AdminName as releaseUser " +
            "from notice, backadmin " +
            "where NoticeTitle like concat('%', #{searchTitle}, '%') " +
            "and N_AdminID = AdminID " +
            "order by ReleaseTime desc " +
            "limit #{pageNumber}, #{pageSize}")
    List<Notice> getNoticeInfo(@Param("pageNumber") Integer pageNumber,
                               @Param("pageSize") Integer pageSize,
                               @Param("searchTitle") String searchTitle);

    //统计公告总数
    @Select("select count(*) from notice where NoticeTitle like concat('%', #{searchTitle}, '%')")
    Integer countAllNoticeTotal(@Param("searchTitle") String searchTitle);

    //删除公告
    @Delete("delete from notice where NoticeID = #{noticeID}")
    Integer deleteNotice(@Param("noticeID") Integer noticeID);

    //插入数据
    @Insert("insert into notice(noticetitle, noticecontent, releasetime, n_adminid) " +
            "values(#{noticeTitle}, #{noticeContent}, current_timestamp, #{adminID}) ")
    Integer addNotice(NoticeDTO noticeDTO);
}
