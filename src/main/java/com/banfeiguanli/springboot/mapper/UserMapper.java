package com.banfeiguanli.springboot.mapper;

import com.banfeiguanli.springboot.entity.ClassInfo;
import com.banfeiguanli.springboot.entity.dto.UserDTO;
import com.banfeiguanli.springboot.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {
    //注册
    @Insert("insert into UserInfo (userName, userAccount, userPWD, userPhone, userEmail) values (#{userName}, #{userAccount}, #{userPWD}, #{userPhone}, #{userEmail})")
    Integer userRegister(UserDTO userDTO);

    //删除用户
    @Delete("delete from userinfo where userID = #{userID}")
    Integer delete(User user);

    //更新用户信息
    Integer updateUserInfo(UserDTO userDTO);

    //查询用户信息
    @Select("select UserID, UI_ClassID as ClassID, UserName, UserAccount, UserPWD, UserPhone,UserEmail, IsBan from userinfo where userID = #{userID}")
    List<User> getUserInfo(Integer userID);

    //登录
    @Select("select userid, username, useraccount, userpwd, userphone, useremail, isban, ui_classid as classid from userinfo where userAccount = #{userAccount} and userPWD = #{userPWD}")
    List<User> userLogin(UserDTO userDTO);
    //检测账号是否存在
    @Select("select * from userinfo where userAccount = #{userAccount}")
    List<User> userAccountExist(String userAccount);
    //检查手机号是否已被注册
    @Select("select * from userinfo where userPhone = #{userPhone}")
    List<User> userPhoneExist(String userPhone);
    //通过用户ID查找班级管理员的用户ID
    @Select("select CA_UserID as userID from userinfo, classadmin where UserID = #{userID} and UI_ClassID = CA_ClassID order by StartTime desc limit 0, 1")
    Integer getClassAdminUserID(@Param(value = "userID") int userID);

    @Select("select className from userinfo,class where UserID = #{userID} and UI_ClassID = ClassID")
    String getUserClassName(@Param(value = "userID") int userID);

    //利用用户ID获取用户已加入的班级信息
    @Select("select classID, className, ClassSize, ClassExBala from userinfo, class where UserID = #{userID} and UI_ClassID = ClassID")
    List<ClassInfo> getUserClassInfo(@Param(value = "userID") int userID);

    //获取用户信息(后台管理)
    @Select("select distinct userID, userName, userInfo.isBan as isBan, classID, className " +
            "from userinfo left outer join class " +
            "on UI_ClassID = ClassID " +
            "and UserName like concat('%', #{searchUser}, '%') " +
            "order by UserID " +
            "limit #{pageNumber}, #{pageSize}")
    List<User> getUserInfoForBack(@Param("pageNumber") Integer pageNumber,
                                  @Param("pageSize") Integer pageSize,
                                  @Param("searchUser") String searchUser);

    //统计总用户数
    @Select("select count(*) from class where ClassName like #{searchUser}")
    Integer countUserTotal(@Param("searchUser") String searchUser);

    //禁封用户
    @Update("update userinfo set IsBan = '是' where UserID = #{userID}")
    Integer banUser(UserDTO userDTO);

    //解封用户
    @Update("update userinfo set IsBan = '否' where UserID = #{userID}")
    Integer unBanUser(UserDTO userDTO);
}
