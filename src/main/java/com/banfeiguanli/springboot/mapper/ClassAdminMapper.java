package com.banfeiguanli.springboot.mapper;

import com.banfeiguanli.springboot.entity.ClassAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ClassAdminMapper {
    @Select("select * from classadmin where CA_ClassID = " +
            "(select UI_ClassID from userinfo where UserID = #{userID}) " +
            "order by StartTime " +
            "desc limit 0, 1")
    List<ClassAdmin> getClassAdmin(@Param(value = "userID") int userID);
}
