package com.banfeiguanli.springboot.mapper;

import com.banfeiguanli.springboot.entity.BackAdmin;
import com.banfeiguanli.springboot.entity.dto.BackAdminDTO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BackAdminMapper {
    //获取后台管理员信息
    @Select("select * from backadmin where AdminID = #{adminID}")
    List<BackAdmin> getBackAdminInfo(int adminID);

    //后台管理员注册
    @Insert("insert into backadmin(adminName, adminAccount, adminPWD, adminPhone, adminEmail) " +
            "values(#{adminName}, #{adminAccount}, #{adminPWD}, #{adminPhone}, #{adminEmail})")
    Integer register(BackAdminDTO backAdminDTO);

    //通过手机号获取后台管理员信息
    @Select("select AdminID from backadmin where AdminPhone = #{adminPhone}")
    List<BackAdmin> getBackAdminInfoByPhone(String adminPhone);

    //通过手机号获取后台管理员信息
    @Select("select AdminID from backadmin where AdminAccount = #{adminAccount}")
    List<BackAdmin> getBackAdminInfoByAccount(String adminAccount);
    //后台管理员登录
    @Select("select * from backadmin where adminAccount = #{adminAccount} and adminPWD = #{adminPWD}")
    List<BackAdmin> login(@Param(value = "adminAccount") String adminAccount, @Param(value = "adminPWD") String adminPWD);

    //利用adminID查询信息
    @Select("select * from backadmin where AdminID = #{adminID}")
    List<BackAdmin> getAdminInfo(@Param("adminID") Integer adminID);
}
